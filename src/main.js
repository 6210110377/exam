const { query } = require('express')
const bodyParser = require('body-parser')
const express = require('express')
const app = express()
const port = 3000

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use('/static',express.static('public'))
app.get('/', (req, res) => {
  res.send('Hello World!')
})
app.get('/add',(req,res) => {
    let number = parseInt(req.query.number)
    let refresh =  parseInt(req.query.refresh)
    res.send(number,refresh)
})
app.post('/add',(req,res) => {
    let number = parseInt(req.body.number)
    let refresh =  parseInt(req.body.refresh)
    res.send(number,refresh)
})

app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})